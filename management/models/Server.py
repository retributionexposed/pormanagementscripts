import logging, sys, os
import copy

class Server(object):

    def __init__(self, dictionary):
        self.type = dictionary.get('type')
        self.baseChannel = dictionary.get('baseChannel')
        self.districtName = dictionary.get('districtName')
        self.astronPath = base.getAstronPath()
        self.logFile = None
        self.emptyLog()
    
    def isReady(self):
        if self.baseChannel is None or self.type is None:
            return False
        if self.type not in base.getServerTypeNames():
            return False
        
        return True
    
    def getStartPath(self):
        path = copy.deepcopy(base.getServerTypes()[self.type]['startPath'])
        
        for i, value in enumerate(path):
            if value.startswith('$'):
                path[i] = unicode(getattr(self, value[1:]))
            elif value.startswith('@'):
                path[i] = unicode(settings[value[1:]])
        
        return path
        
    def getType(self):
        return self.type

    def getBaseChannel(self):
        return self.baseChannel
    
    def getDistrictName(self):
        name = copy.deepcopy(base.getServerTypes()[self.type]['name'])
        
        if name.startswith('$'):
            name = str(getattr(self, name[1:]))
        
        return name
    
    def setDistrictName(self, districtName):
        self.districtName = districtName
    
    def getLog(self):
        return self.log
    
    def isStarted(self):
        return base.aiManager.isStarted(self.baseChannel)
    
    def info(self, message):
        logging.info('[%s] %s' % (self.getDistrictName(), message))
    
    def emptyLog(self):
        self.log = ''
    
    def appendLog(self, message):
        self.log += message
        
        if self.logFile:
            self.logFile.write(message.replace('\r', ''))
            self.logFile.flush()
    
    def cycleLogFile(self):
        if self.logFile:
            self.logFile.close()
        
        filename = base.getLogFilename(self.getDistrictName())
        
        mode = 'w' if not os.path.exists(filename) else 'a'
        self.logFile = open(filename, mode)