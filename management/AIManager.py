from management.util import Utils
from AIThread import AIThread
import logging, time

class AIManager(object):

    def __init__(self, servers):
        self.servers = servers
        self.threads = {}
    
    def getServers(self):
        return self.servers
    
    def getThreads(self):
        return self.threads
    
    def getThread(self, server):
        return self.threads.get(server.getBaseChannel())
    
    def isStarted(self, channel):
        return channel in self.threads
    
    def isExistingChannel(self, channel):
        return self.getServerByChannel(channel) is not None
    
    def getServerByChannel(self, channel):
        for server in self.servers:
            if server.getBaseChannel() == channel:
                return server
    
    def startServer(self, server):
        channel = server.getBaseChannel()

        if channel not in self.threads:
            thread = AIThread(server)
            self.threads[channel] = thread
            thread.start()
    
    def stopServer(self, server):
        channel = server.getBaseChannel()
        
        if channel in self.threads:
            self.threads[channel].stop()
            del self.threads[channel]
    
    def addServer(self, server):
        if server not in self.servers:
            server.info("Adding...")
            self.servers.append(server)
            self.startServer(server)
    
    def removeServer(self, server):
        if server in self.servers:
            server.info("Removing...")
            self.servers.remove(server)
            self.stopServer(server)
    
    def start(self):
        for type in base.getServerTypeNames():
            for server in self.servers:
                if server.getType() == type:
                    self.startServer(server)

            time.sleep(0.25)