from management.util.Settings import Settings
import argparse

parser = argparse.ArgumentParser(description='Setup the ODS management server.')
parser.add_argument('--stateserver', metavar=1100, help="The control channel of this UD's designated State Server.")
parser.add_argument('--astron-ip', metavar='127.0.0.1:29170', help='The IP address of the Astron Message Director to connect to.')
parser.add_argument('--eventlogger-ip', metavar='127.0.0.1:29160', help='The IP address of the Astron Event Logger to log to.')
args = vars(parser.parse_args())

for arg in args.values():
    if not arg:
        parser.error('Argument not provided.')

keys = [
    ('stateServer', 'stateserver', int),
    ('astronIp', 'astron_ip', str),
    ('eventLoggerIp', 'eventlogger_ip', str),
]

try:
    store = {k: type(args[k2]) for k, k2, type in keys}
except:
    parser.error('Invalid arguments given.')

settings = Settings('settings.json')
settings.store = store
settings.write()
print 'Saved.'