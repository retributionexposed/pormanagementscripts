from management.util.SSLThreadedServer import SSLThreadedServer
from management.models.Server import Server
from management.util import Utils
from BackupThread import BackupThread
from DeployThread import DeployThread
from SocketServer import StreamRequestHandler
import ManagerGlobals
import subprocess, urllib2, ast, copy, json, logging, os, time, ssl, sys

class RPCServerHandler(StreamRequestHandler):
    timeout = 120.0
    ignoreExceptions = False
    
    def getAddress(self):
        return self.client_address[0]
    
    def readLine(self):
        return self.rfile.readline().strip()
    
    def sendDictionary(self, dictionary):
        self.wfile.write(json.dumps(dictionary) + '/x08')
    
    def sendMessage(self, success, message, extraData=[]):
        self.sendDictionary({'operation': self.operation, 'success': success, 'message': message, 'extraData': extraData})
    
    def readChannels(self, default=[]):
        try:
            return ast.literal_eval(self.readLine())
        except:
            return default
    
    def handle(self):
        if self.ignoreExceptions:
            try:
                self.__handle()
            except:
                pass
        else:
            self.__handle()
    
    def finish(self):
        StreamRequestHandler.finish(self)
        self.server.delBusy(self.getAddress())
    
    def writeLog(self, message):
        base.getLogFile().write('%s %s\n' % (time.strftime('[%Y/%m/%d %H:%M:%S]'), message))
        base.getLogFile().flush()
    
    def __handle(self):
        self.server.addBusy(self.getAddress())

        self.operation = int(self.readLine())
        self.key = self.readLine()
        keys = base.getPrivateKeys()
        
        if len(keys) > 0 and self.key not in keys:
            self.writeLog('%s tried to log in with key "%s"' % (self.getAddress(), self.key))
            self.server.addCooldown(self.getAddress(), 10)
            self.sendMessage(False, 'Unauthorized.')
            return

        keyDoc = keys[self.key]
        cmdName = ManagerGlobals.COMMAND_NAMES.get(self.operation, 0)

        if 'deniedCommands' in keyDoc and cmdName in keyDoc['deniedCommands']:
            self.writeLog('%s tried to use denied command "%s" with key "%s"' % (self.getAddress(), cmdName, self.key))
            self.sendMessage(False, 'This command is currently unavailable.')
            return

        if self.operation == ManagerGlobals.CREATE_SERVER:
            self.__createServer()
        elif self.operation == ManagerGlobals.DELETE_SERVERS:
            self.__deleteServers()
        elif self.operation == ManagerGlobals.START_SERVERS:
            self.__startServers()
        elif self.operation == ManagerGlobals.STOP_SERVERS:
            self.__stopServers()
        elif self.operation == ManagerGlobals.LIST_SERVERS:
            self.__listServers()
        elif self.operation == ManagerGlobals.REQUEST_LOG:
            self.__requestLog()
        elif self.operation == ManagerGlobals.RENAME_AI:
            self.__renameAI()
        elif self.operation == ManagerGlobals.REQUEST_OPTIONS:
            self.__requestOptions()
        elif self.operation == ManagerGlobals.SET_OPTIONS:
            self.__setOptions()
        elif self.operation == ManagerGlobals.EXEC_CODE:
            self.__execCode()
        elif self.operation == ManagerGlobals.UPDATE_SELF:
            self.__updateSelf()
        elif self.operation == ManagerGlobals.DEPLOY_GAME:
            self.__deployGame()
        elif self.operation == ManagerGlobals.BACKUP_DATABASE:
            self.__backupDatabase()
        elif self.operation == ManagerGlobals.GET_DATABASE_BACKUPS:
            self.__getDatabaseBackups()
        elif self.operation == ManagerGlobals.REQUEST_SUBPROCESS_LOG:
            self.__requestSubprocessLog()
        elif self.operation == ManagerGlobals.DOWNLOAD_CERTIFICATES:
            self.__downloadCertificates()
        else:
            self.writeLog('%s used incorrect operation with key "%s"' % (self.getAddress(), self.key))
            self.sendMessage(False, 'Invalid operation.')
    
    def __createServer(self):
        channel = int(self.readLine())
        
        if base.aiManager.isExistingChannel(channel):
            self.sendMessage(False, 'That channel already exists!')
            return
        
        type = self.readLine()
        
        if type not in base.getServerTypeNames():
            self.sendMessage(False, 'That server type is not implemented!')
            return
        
        districtName = self.readLine()
        dictionary = {'type': type, 'baseChannel': channel, 'districtName': districtName}

        if isinstance(servers.store, dict):
            servers.store = []

        servers.store.append(dictionary)
        servers.write()

        base.aiManager.addServer(Server(dictionary))
        self.writeLog('%s created server with key "%s" (%s, %s, %s)' % (self.getAddress(), self.key, type, channel, districtName))
        self.sendMessage(True, 'Successfully created your server!')
    
    def __startServers(self):
        channels = self.readChannels()
        done = 0
        
        for channel in channels:
            if not base.aiManager.isExistingChannel(channel):
                continue

            if not base.aiManager.isStarted(channel):
                base.aiManager.startServer(base.aiManager.getServerByChannel(channel))

            done += 1

        self.writeLog('%s started servers up with key "%s" (%s)' % (self.getAddress(), self.key, channels))
        self.sendMessage(True, 'Successfully booted %d/%d servers up!' % (done, len(channels)))
    
    def __stopServers(self):
        channels = self.readChannels()
        done = 0
        
        for channel in channels:
            if not base.aiManager.isExistingChannel(channel):
                continue

            if base.aiManager.isStarted(channel):
                base.aiManager.stopServer(base.aiManager.getServerByChannel(channel))
            
            done += 1

        self.writeLog('%s shut servers down with key "%s" (%s)' % (self.getAddress(), self.key, channels))
        self.sendMessage(True, 'Successfully stopped %d/%d servers!' % (done, len(channels)))
    
    def __deleteServers(self):
        channels = self.readChannels()
        done = 0
        
        for channel in channels:
            if not base.aiManager.isExistingChannel(channel):
                continue
        
            server = base.aiManager.getServerByChannel(channel)
        
            for i, dict in enumerate(servers.store):
                if dict['baseChannel'] == channel:
                    servers.store.remove(dict)
            
            base.aiManager.removeServer(server)
            done += 1
        
        servers.write()
        self.writeLog('%s deleted servers with key "%s" (%s)' % (self.getAddress(), self.key, channels))
        self.sendMessage(True, 'Successfully deleted %d/%d servers!' % (done, len(channels)))
    
    def __listServers(self):
        store = copy.copy(servers.store)
        
        for server in store:
            server['started'] = base.aiManager.isStarted(server['baseChannel'])

        self.sendMessage(True, 'Here you go!', store)
    
    def __requestLog(self):
        channel = int(self.readLine())
        
        if not base.aiManager.isExistingChannel(channel):
            self.sendMessage(False, 'That channel does not exist!')
            return
        
        server = base.aiManager.getServerByChannel(channel)
        self.writeLog('%s requested the log for %s with key "%s"' % (self.getAddress(), server.getDistrictName(), self.key))
        self.sendMessage(True, 'Here you go!', server.getLog())
    
    def __renameAI(self):
        channel = int(self.readLine())
        
        if not base.aiManager.isExistingChannel(channel):
            self.sendMessage(False, 'That channel does not exist!')
            return
        
        server = base.aiManager.getServerByChannel(channel)
        
        if server.getType() != 'ai':
            self.sendMessage(False, 'This server is not a district!')
            return
        
        districtName = self.readLine()
        
        server.setDistrictName(districtName)
        
        if base.aiManager.isStarted(channel):
            base.aiManager.stopServer(server)
            base.aiManager.startServer(server)
        
        for i, dict in enumerate(servers.store):
            if dict['baseChannel'] == channel:
                dict['districtName'] = districtName
        
        servers.write()
        self.writeLog('%s renamed the district %s with key "%s"' % (self.getAddress(), districtName, self.key))
        self.sendMessage(True, 'Successfully renamed that district to %s!' % districtName)
    
    def __requestOptions(self):
        self.writeLog('%s requested settings with key "%s"' % (self.getAddress(), self.key))
        self.sendMessage(True, 'Here you go!', settings.store)
    
    def __setOptions(self):
        try:
            options = self.readChannels()
        except:
            self.sendMessage(False, 'Please re-check your input!')
            return
        
        if not options or not isinstance(options, dict):
            self.sendMessage(False, 'Please re-check your input!')
            return

        settings.store = options
        settings.write()
        self.writeLog('%s set settings with key "%s" (%s)' % (self.getAddress(), self.key, options))
        self.sendMessage(True, 'Your options are saved. Please make sure to update the utility!')
    
    def __execCode(self):
        try:
            code = self.readLine().replace('/x09', '\n').replace('\r', '')
            exec code
            self.writeLog('%s executed code with key "%s" (%s)' % (self.getAddress(), self.key, code))
            self.sendMessage(True, 'The code has been executed.')
        except Exception as e:
            self.sendMessage(False, e.message or 'A syntax error has happened.')
    
    def __updateSelf(self):        
        git = ['git', '-C', base.getDirectory()]
        
        logging.info("Updating self...")
        Utils.runSubprocess(git + ['reset', '--hard'])
        Utils.runSubprocess(git + ['pull'])
        self.writeLog('%s updated self with key "%s"' % (self.getAddress(), self.key))
        self.sendMessage(True, 'The program has been updated.')
        
        os._exit(1)
    
    def __getDatabaseBackups(self):
        if not base.hasMega():
            self.sendMessage(False, 'Backups are disabled.')
            return

        self.writeLog('%s got database backups with key "%s"' % (self.getAddress(), self.key))

        try:
            files = base.mega.listFiles('/Root/Backups', downloadLinks=True)
        except:
            files = []

        files = sorted(files, key=lambda f: f['timestamp'], reverse=True)
        
        extraData = []
        
        for file in files:
            extraData.append('[%s %s] %s' % (file['date'], file['time'], file['link']))
        
        self.sendMessage(True, 'Here you go!', '\n'.join(extraData))
    
    def __backupDatabase(self):
        if not base.hasMega():
            self.sendMessage(False, 'Backups are disabled.')
            return

        if base.isBackupRunning():
            self.sendMessage(False, 'A backup process is already underway.')
            return

        self.writeLog('%s started database backup with key "%s"' % (self.getAddress(), self.key))
        base.startBackup()
        self.sendMessage(True, 'Backup started!')

    def __deployGame(self):
        if not settings.get('isDeployServer', False):
            self.sendMessage(False, 'This server is not set up for deployment.')
            return
        
        if base.deployThread:
            self.sendMessage(False, 'A deployment process is already underway.')
            return

        directory = base.getDirectoryPath('build')
        
        if not os.path.exists(directory):
            self.sendMessage(False, 'The build directory is missing.')
            return
        
        makeScript = os.path.join(directory, 'make.py')
        
        if not os.path.exists(makeScript):
            self.sendMessage(False, 'The build script is missing.')
            return
        
        base.deployThread = DeployThread()
        base.deployThread.start()
        self.writeLog('%s deployed game with key "%s"' % (self.getAddress(), self.key))
        self.sendMessage(True, 'Deployment started!')
    
    def __requestSubprocessLog(self):
        self.writeLog('%s requested subprocess log with key "%s"' % (self.getAddress(), self.key))
        self.sendMessage(True, 'Here you go!', base.getSubprocessLog())

    def __downloadCertificates(self):
        certificateUrl = self.readLine()
        certificateKeyUrl = self.readLine()
        
        try:
            with Utils.closing(urllib2.urlopen(certificateUrl)) as connection:
                certificate = connection.read()
        except:
            self.sendMessage(False, "Couldn't download certificate!")
            return
        
        try:
            with Utils.closing(urllib2.urlopen(certificateKeyUrl)) as connection:
                certificateKey = connection.read()
        except:
            self.sendMessage(False, "Couldn't download certificate key!")
            return
        
        certificateFolder = base.getDirectoryPath('astron-certs')
        
        with open(os.path.join(certificateFolder, 'cert.crt'), 'w') as certificateFile:
            certificateFile.write(certificate)
        
        with open(os.path.join(certificateFolder, 'cert.key'), 'w') as certificateKeyFile:
            certificateKeyFile.write(certificateKey)
        
        self.writeLog('%s downloaded certificates with key "%s" (%s, %s)' % (self.getAddress(), self.key, certificateUrl, certificateKeyUrl))
        self.sendMessage(True, "Successfully updated certificates!")
    
class RPCServer(SSLThreadedServer):
    allow_reuse_address = True

    def __init__(self, server_address, requestHandlerClass, certFile, keyFile, sslVersion=ssl.PROTOCOL_TLSv1_2, bind_and_activate=False):
        SSLThreadedServer.__init__(self, server_address, requestHandlerClass, certFile, keyFile, sslVersion, bind_and_activate)
        self.cooldowns = {}
        self.busyIps = []
    
    def isBusy(self, ip):
        return ip in self.busyIps or (ip in self.cooldowns and self.cooldowns[ip] > time.time())
    
    def addBusy(self, ip):
        if ip not in self.busyIps:
            self.busyIps.append(ip)
    
    def delBusy(self, ip):
        if ip in self.busyIps:
            self.busyIps.remove(ip)
    
    def addCooldown(self, ip, cooldown):
        oldCooldown = self.cooldowns.get(ip)
        
        cooldownBase = time.time() if (not oldCooldown) or oldCooldown < time.time() else oldCooldown
        self.cooldowns[ip] = cooldownBase + cooldown
    
    def verify_request(self, request, client_address):
        return not self.isBusy(client_address[0])