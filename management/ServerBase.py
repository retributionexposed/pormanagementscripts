from management.util.Settings import Settings
from management.models.Server import Server
from management.util import Utils
from AIManager import AIManager
from RPCServer import RPCServerHandler, RPCServer
from BackupThread import BackupThread
from PeriodicBackupThread import PeriodicBackupThread
from MegaService import MegaService
import shutil, logging, time, sys, os, re
import __builtin__

class ServerBase(object):

    def __init__(self):
        if not os.path.exists('ca.crt') or not os.path.exists('ca.key'):
            logging.error('SSL certificate is missing.')
            return

        self.directory = os.getcwd()
        self.servers = []
        self.deployThread = None
        self.backupThread = None
        self.mega = None

        self.subprocessLog = ''

        logging.info('Loading settings...')
        __builtin__.settings = Settings(os.path.join(self.directory, 'settings.json'))
        __builtin__.servers = Settings(os.path.join(self.directory, 'servers.json'))
    
    def isBackupRunning(self):
        return self.backupThread is not None
    
    def startBackup(self):
        if not self.backupThread:
            self.backupThread = BackupThread()
            self.backupThread.start()
    
    def getMega(self):
        return self.mega
    
    def hasMega(self):
        return self.mega is not None
    
    def getDirectory(self):
        return self.directory

    def getServers(self):
        return self.servers
    
    def getDirectories(self):
        return settings['directories']

    def getRepositories(self):
        return settings['repositories']
    
    def getServerTypes(self):
        return settings['serverTypes']
    
    def getServerTypeNames(self):
        return settings['serverTypesOrder']
    
    def getDirectoryPath(self, name):
        path = self.getDirectories()[name]
        
        if path.startswith('./'):
            path = os.path.join(self.directory, path[2:])

        return path

    def getAstronPath(self):
        filename = settings['astronFilenames'].get(sys.platform, 'astrond.exe')
        return os.path.join(self.getDirectoryPath('astron'), filename)

    def getGameBranch(self):
        return settings['gameBranch']
    
    def getPrivateKeys(self):
        return settings.get('privateKeys', {})
    
    def getSubprocessLog(self):
        return self.subprocessLog
    
    def appendSubprocessLog(self, message):
        self.subprocessLog += message
    
    def fillClusterTemplate(self, clusterTemplate):
        replacements = re.findall("[^[]*\[([^]]*)\]", clusterTemplate)
        
        for replacement in replacements:
            if replacement.startswith('$'):
                value = str(settings[replacement[1:]])
            elif replacement.startswith('@'):
                value = str(Utils.getPort(settings[replacement[1:]]))

            clusterTemplate = clusterTemplate.replace('[%s]' % replacement, value)
        
        return clusterTemplate

    def updateRepository(self, repository):
        name = repository['name']
        directory = self.getDirectoryPath(repository['directory'])
        
        if (not os.path.exists(os.path.join(directory, '.git'))) and os.path.exists(directory):
            shutil.rmtree(directory)

        if not os.path.exists(directory):
            os.makedirs(directory)

        git = ['git', '-C', directory]

        if not os.path.exists(os.path.join(directory, '.git')):
            logging.info('Cloning %s...' % name)
            Utils.runSubprocess(git + ['clone', repository['git'], '.'])
        else:
            logging.info('Updating %s...' % name)
            Utils.runSubprocess(git + ['remote', 'set-url', 'origin', repository['git']])
            Utils.runSubprocess(git + ['pull'])

        if 'branch' in repository:
            branch = repository['branch']
            
            if branch != 'master':
                logging.info('Checking out %s...' % branch)
            
            Utils.runSubprocess(git + ['checkout', branch])
        
        if 'commit' in repository:
            commit = repository['commit']

            logging.info('Skipping to commit %s...' % commit)
            Utils.runSubprocess(git + ['reset', '--hard', commit])
        else:
            logging.info("Skipping to most recent commit...")
            Utils.runSubprocess(git + ['reset', '--hard'])

    def updateRepositories(self):
        for repository in self.getRepositories():
            self.updateRepository(repository)

        configDirectory = self.getDirectoryPath('astron-config')
        
        for file in settings['astronConfigFiles']:
            templateName = os.path.join(configDirectory, file + '_tmp.yml')
            
            if not os.path.exists(templateName):
                logging.warning('%s is missing.' % file)
                continue
            
            resultName = os.path.join(configDirectory, file + '.yml')
            logging.info('Created config file %s!' % (file + '.yml'))
            
            with open(templateName, 'r') as templateFile:
                template = templateFile.read()
                
                with open(resultName, 'w') as resultFile:
                    resultFile.write(self.fillClusterTemplate(template))

    def getLogFilename(self, logName):
        logDirectory = os.path.join(self.logDirectory, logName)
        
        if not os.path.exists(logDirectory):
            os.makedirs(logDirectory)
        
        return os.path.join(logDirectory, time.strftime('%Y%m%d-%H%M%S.log'))
    
    def getLogFile(self):
        return self.logFile

    def run(self):
        if not hasattr(self, 'directory'):
            return

        self.logDirectory = self.getDirectoryPath('logs')
        
        if not os.path.exists(self.logDirectory):
            os.makedirs(self.logDirectory)
        
        filename = base.getLogFilename('RPC')
        mode = 'w' if not os.path.exists(filename) else 'a'
        self.logFile = open(filename, mode)
        
        if 'megaEmail' in settings and 'megaPassword' in settings:
            self.mega = MegaService(settings['megaEmail'], settings['megaPassword'], settings.get('megaSpace', -1))
            logging.info('Logged into MEGA.')
        else:
            self.mega = None
            logging.info('MEGA is unavailable.')

        for server in servers:
            server = Server(server)
            
            if server.isReady():
                self.servers.append(server)
                logging.info('Loaded server "%s" (channel %d)!' % (server.getDistrictName(), server.getBaseChannel()))
            else:
                logging.warning('Server is incomplete (channel %d).' % server.getBaseChannel())
                continue

        self.aiManager = AIManager(self.servers)
        self.rpcServer = RPCServer((settings['bindIp'], settings['bindPort']), RPCServerHandler, os.path.join(self.directory, 'ca.crt'), \
            os.path.join(self.directory, 'ca.key'), bind_and_activate=False)

        self.updateRepositories()
        logging.info('Starting servers...')
        self.aiManager.start()

        if self.hasMega() and 'backupInterval' in settings:
            logging.info('Starting periodic backup...')
            self.periodicBackupThread = PeriodicBackupThread()
            self.periodicBackupThread.start()
        else:
            logging.info('Not starting periodic backup.')

        self.rpcServer.server_bind()
        self.rpcServer.server_activate()
        self.rpcServer.serve_forever(0.01)
