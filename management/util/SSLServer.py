from SocketServer import TCPServer
import ssl

class SSLServer(TCPServer):

    def __init__(self, server_address, requestHandlerClass, certFile, keyFile, sslVersion=ssl.PROTOCOL_TLSv1_2, bind_and_activate=True):
        TCPServer.__init__(self, server_address, requestHandlerClass, bind_and_activate)
        self.certFile = certFile
        self.keyFile = keyFile
        self.sslVersion = sslVersion

    def get_request(self):
        socket, fromAddr = self.socket.accept()
        socket = ssl.wrap_socket(socket, server_side=True, certfile = self.certFile, keyfile = self.keyFile, ssl_version = self.sslVersion)
        return socket, fromAddr