import zipfile
import StringIO

class InMemoryZip(object):

    def __init__(self):
        self.memoryZip = StringIO.StringIO()

    def append(self, filename, contents):
        file = zipfile.ZipFile(self.memoryZip, 'a', zipfile.ZIP_DEFLATED, False)
        file.writestr(filename, contents)

        for zFile in file.filelist:
            zFile.create_system = 0        

        return self
    
    def appendFile(self, filename):
        file = zipfile.ZipFile(self.memoryZip, 'a', zipfile.ZIP_DEFLATED, False)
        file.write(filename)
        
        for zFile in file.filelist:
            zFile.create_system = 0
        
        return self

    def read(self):
        self.memoryZip.seek(0)
        return self.memoryZip.read()
